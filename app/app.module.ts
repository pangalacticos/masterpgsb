import { enableProdMode }       from '@angular/core';
import { NgModule }             from '@angular/core';
import { BrowserModule }        from '@angular/platform-browser';
import { FormsModule }          from '@angular/forms';
import { HttpModule, JsonpModule }           from '@angular/http';

import { InMemoryWebApiModule } from 'angular2-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { routing }              from './app.routing';
import { AppComponent }         from './app.component';
import { CabecalhoComponent }   from './cabecalho/cabecalho.component';
import { PainelComponent }      from './painel/painel.component';
import { InicialComponent }     from './inicial/inicial.component';
import { LivroComponent }       from './livro/livro.component';
import { PersonagemComponent }  from './personagem/personagem.component';
import { AventuraComponent }    from './aventura/aventura.component';
import { LivroDetalheComponent }        from './livro/livro-detalhe.component';
import { AtributoVisualizarComponent }  from './livro/atributo/atributo-visualizar.component';
import { AtributoCadastrarComponent }   from './livro/atributo/atributo-cadastrar.component';
import { AtributoDetailComponent }   from './livro/atributo/atributo-detail.component';
import { AtributoService }   from './livro/atributo/atributo.service';

enableProdMode();

@NgModule({
    imports: [ 
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        routing
    ],
    declarations:   [ 
        AppComponent,

        CabecalhoComponent,
        PainelComponent,

        InicialComponent,

        PersonagemComponent,

        AventuraComponent,

        LivroComponent,
        LivroDetalheComponent,
        AtributoVisualizarComponent,
        AtributoCadastrarComponent,

        AtributoDetailComponent
    ],
    providers:[
        AtributoService
    ],
    bootstrap:      [ AppComponent ]
})
export class AppModule { }