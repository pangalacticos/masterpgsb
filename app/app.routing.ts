import { ModuleWithProviders}   from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InicialComponent }     from './inicial/inicial.component';
import { PersonagemComponent }  from './personagem/personagem.component';
import { AventuraComponent }    from './aventura/aventura.component';

import { LivroComponent }       from './livro/livro.component';
import { LivroDetalheComponent }       from './livro/livro-detalhe.component';
import { AtributoVisualizarComponent }  from './livro/atributo/atributo-visualizar.component';
import { AtributoCadastrarComponent }   from './livro/atributo/atributo-cadastrar.component';


const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/inicial',
        pathMatch: 'full'
    },
    {
        path: 'inicial',
        component: InicialComponent
    },
    {
        path: 'personagem',
        component: PersonagemComponent
    },
    {
        path: 'aventura',
        component: AventuraComponent
    },
    {
        path: 'livro',
        component: LivroComponent
    },
    {
        path: 'livro/detalhe',
        component: LivroDetalheComponent
    },
    {
        path: 'livro/detalhe/atributo',
        component: AtributoVisualizarComponent
    },
    {
        path: 'livro/detalhe/atributo/cadastrar',
        component: AtributoCadastrarComponent
    },
    {
        path: 'livro/detalhe/atributo/:id',
        component: AtributoCadastrarComponent
    }
];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);