import { Injectable }   from '@angular/core';

import { Livro }        from './livro';
import { LIVROS }       from './mock-livros';

@Injectable()
export class LivroService{
    getLivros(): Promise<Livro[]> {
        return Promise.resolve(LIVROS);
    }
}