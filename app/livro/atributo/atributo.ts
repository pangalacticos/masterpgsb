//import { AtributoDependente } from './atributodependente.ts';
export class Atributo {
    id: number;
    nome: string;
    sigla: string;
    descricao: string;
    dinamico: boolean;
    //dependente : Array<AtributoDependente>;
};

export class AtributoDependente {
    baseId: number;
    operador: number;
    multiadd: boolean;
};