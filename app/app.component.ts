import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `

            <div id="wrapper">
                <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <!-- Usuário -->
                    <cabecalho></cabecalho>
                    <!-- Barra Lateral -->
                    <painel-lateral></painel-lateral>
                </nav>
                <div id="page-wrapper">
                    <router-outlet></router-outlet>
                </div>
            </div>

    `
})
export class AppComponent {}