import { Component } from '@angular/core';

@Component({
    selector: 'painel-lateral',
    template: `
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" 
            id="side-menu">
                <li>
                    <a routerLink="/inicial"><i class="fa fa-globe fa-fw"></i> Master</a>
                </li>
                <li>
                    <a routerLink="/aventura"><i class="fa fa-map fa-fw"></i> Aventuras</a>
                </li>
                <li>
                    <a routerLink="/personagem"><i class="fa fa-street-view fa-fw"></i> Personagens</a>
                </li>
                <li>
                    <a routerLink="/livro"><i class="fa fa-book fa-fw"></i> Livros</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="blank.html">Blank Page</a>
                        </li>
                        <li>
                            <a href="login.html">Login Page</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    `
})
export class PainelComponent {}