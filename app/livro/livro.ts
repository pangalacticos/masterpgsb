export class Livro {
    id: number;
    nome: string;
    versao: string;
    qtd_criaturas: number;
    qtd_itens: number;
    qtd_regras: number;
}